CREATE DATABASE IF NOT EXISTS `final_symphony`;
USE `final_symphony`;

CREATE TABLE `customers` (
	`Id`	BIGINT UNSIGNED AUTO_INCREMENT,
	`Name`	VARCHAR(256) NOT NULL,
	`Status` INT NOT NULL,
	
	`DateCreated`	DATETIME NOT NULL,
	`DateModified`	DATETIME DEFAULT NULL,
	
	PRIMARY KEY (`Id`)
 )engine=innodb default charset=utf8mb4;
 
 CREATE TABLE `documents` (
	`Id`	BIGINT UNSIGNED AUTO_INCREMENT,
	`DataJson`	LONGTEXT,
	`CustomerId` BIGINT NOT NULL,
	
	`DateCreated`	DATETIME NOT NULL,
    
    PRIMARY KEY (`Id`)
 )engine=innodb default charset=utf8mb4;