﻿namespace FinalSymphony.Data.Repositories.Primitives
{
    using Dapper;
    using FinalSymphony.Models.Contracts.Data;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public abstract class BaseRepository
    {
        private readonly IDbConnectionFactory _dbConnectionFactory;

        protected BaseRepository(IDbConnectionFactory dbConnectionFactory)
        {
            _dbConnectionFactory = dbConnectionFactory;
        }

        protected async Task Execute(string sql, object parameters)
        {
            using (var conn = _dbConnectionFactory.GetWrite())
                await conn.ExecuteAsync(sql, parameters);
        }

        protected async Task<IEnumerable<T>> WriteAndQuery<T>(string sql, object parameters)
        {
            using (var conn = _dbConnectionFactory.GetWrite())
                return await conn.QueryAsync<T>(sql, parameters);
        }

        protected async Task<T> WriteAndQuerySingleOrDefault<T>(string sql, object parameters)
        {
            using (var conn = _dbConnectionFactory.GetWrite())
                return await conn.QuerySingleOrDefaultAsync<T>(sql, parameters);
        }

        protected async Task<IEnumerable<T>> Query<T>(string sql, object parameters)
        {
            using (var conn = _dbConnectionFactory.GetRead())
                return await conn.QueryAsync<T>(sql, parameters);
        }

        protected async Task<T> QuerySingleOrDefault<T>(string sql, object parameters)
        {
            using (var conn = _dbConnectionFactory.GetRead())
                return await conn.QuerySingleOrDefaultAsync<T>(sql, parameters);
        }
    }
}