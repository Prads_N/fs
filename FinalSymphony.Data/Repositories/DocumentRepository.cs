﻿namespace FinalSymphony.Data.Repositories
{
    using Primitives;
    using Models.Contracts.Data.Repositories;
    using FinalSymphony.Models.Contracts.Data;
    using System.Threading.Tasks;
    using FinalSymphony.Models.Data;
    using FinalSymphony.Models;
    using System.Collections.Generic;

    public class DocumentRepository : BaseRepository, IDocumentRepository
    {
        public DocumentRepository(IDbConnectionFactory dbConnectionFactory) : base(dbConnectionFactory) { }

        public async Task<DocumentDataModel> Insert(DocumentDataModel model)
        {
            var sql = $@"INSERT INTO {Constants.DatabaseTables.Documents} (`DataJson`,`CustomerId`,`DateCreated`) VALUES (@DataJson,@CustomerId,UTC_TIMESTAMP());
                        SELECT LAST_INSERT_ID();";

            model.Id = await WriteAndQuerySingleOrDefault<ulong>(sql, model);
            return model;
        }

        public Task<IEnumerable<DocumentDataModel>> Get(ulong customerId, IEnumerable<ulong> documentIds)
        {
            var sql = $@"SELECT * FROM {Constants.DatabaseTables.Documents} WHERE `Id` IN @documentIds AND `CustomerId`=@customerId;";
            return Query<DocumentDataModel>(sql, new { customerId, documentIds });
        }
    }
}