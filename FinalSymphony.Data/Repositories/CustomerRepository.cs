﻿namespace FinalSymphony.Data.Repositories
{
    using Models.Data;
    using Models.Contracts.Data;
    using Models.Contracts.Data.Repositories;
    using Primitives;
    using System.Threading.Tasks;
    using FinalSymphony.Models;

    public class CustomerRepository : BaseRepository, ICustomerRepository
    {
        public CustomerRepository(IDbConnectionFactory dbConnectionFactory) : base(dbConnectionFactory) { }

        public async Task<Customer> Insert(Customer customer)
        {
            var sql = $@"INSERT INTO {Constants.DatabaseTables.Customers} (`Name`,`Status`,`Index`,`DateCreated`) VALUES (@Name,@Status,@Index,UTC_TIMESTAMP()); 
                         SELECT INSERT_ID();";

            customer.Id = await WriteAndQuerySingleOrDefault<ulong>(sql, customer);
            return customer;
        }

        public Task<Customer> Get(ulong id)
        {
            var sql = $@"SELECT * FROM {Constants.DatabaseTables.Customers} WHERE `Id`=@id;";
            return QuerySingleOrDefault<Customer>(sql, new { id });
        }
    }
}