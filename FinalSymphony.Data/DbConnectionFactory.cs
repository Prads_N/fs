﻿namespace FinalSymphony.Data
{
    using Models.Contracts.Data;
    using MySql.Data.MySqlClient;
    using System.Data;

    public class DbConnectionFactory : IDbConnectionFactory
    {
        private readonly string _readConnectionString;
        private readonly string _writeConnectionString;

        public DbConnectionFactory(string readConnectionString, string writeConnectionString)
        {
            _readConnectionString = readConnectionString;
            _writeConnectionString = writeConnectionString;
        }

        public IDbConnection GetRead()
        {
            return new MySqlConnection(_readConnectionString);
        }

        public IDbConnection GetWrite()
        {
            return new MySqlConnection(_writeConnectionString);
        }
    }
}