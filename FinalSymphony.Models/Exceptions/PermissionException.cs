﻿namespace FinalSymphony.Models.Exceptions
{
    using System;

    public class PermissionException : ClientErrorException
    {
        public PermissionException(string message) : base(message) { }
        public PermissionException(string message, Exception innerException) : base(message, innerException) { }
    }
}