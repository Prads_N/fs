﻿namespace FinalSymphony.Models.Exceptions
{
    using System;

    public class ClientErrorException : Exception
    {
        public ClientErrorException(string message) : base(message) { }
        public ClientErrorException(string message, Exception innerException) : base(message, innerException) { }
    }
}