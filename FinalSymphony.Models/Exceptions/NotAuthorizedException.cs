﻿namespace FinalSymphony.Models.Exceptions
{
    using System;

    public class NotAuthorizedException : Exception
    {
        public NotAuthorizedException() : base("Not authorised to access the resource") { }
        public NotAuthorizedException(string message) : base(message) { }
        public NotAuthorizedException(string message, Exception innerException) : base(message, innerException) { }
    }
}