﻿namespace FinalSymphony.Models.Data.Enums
{
    public enum CustomerStatus
    {
        Active = 1,
        InActive = 2
    }
}