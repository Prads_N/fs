﻿namespace FinalSymphony.Models.Data
{
    using System;

    public class DocumentDataModel
    {
        public ulong Id { get; set; }
        public string DataJson { get; set; }
        public ulong CustomerId { get; set; }
        public DateTime DateCreated { get; set; }
    }
}