﻿namespace FinalSymphony.Models.Data
{
    using Enums;

    public class Customer
    {
        public ulong Id { get; set; }
        public string Name { get; set; }
        public CustomerStatus Status { get; set; }
        public string Index { get; set; }
    }
}