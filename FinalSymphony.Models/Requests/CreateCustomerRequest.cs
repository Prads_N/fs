﻿namespace FinalSymphony.Models.Requests
{
    using Data.Enums;

    public class CreateCustomerRequest
    {
        public string Name { get; set; }
        public CustomerStatus Status { get; set; }
    }
}