﻿namespace FinalSymphony.Models.Requests
{
    using FinalSymphony.Models.Engine.Request;

    public class DataStorageRequest
    {
        public DataStorageEngineRequest EngineRequest { get; set; }
        public string DataJson { get; set; }
    }
}