﻿namespace FinalSymphony.Models
{
    public static class Constants
    {
        public static class DatabaseTables
        {
            private const string Schema = "`final_symphony`";

            public const string Customers = Schema + ".`customers`";
            public const string Documents = Schema + ".`documents`";
        }

        public static class Auth
        {
            public static class Scopes
            {
                public const string Engine = "fs_engine";
                public const string Client = "fs_client";
            }

            public static class ApiNames
            {
                public const string Client = "fs_client";
            }

            public static class Claims
            {
                public const string CustomerId = "customer_id";
            }
        }

        public static class Elastic
        {
            public const string Index = "edocs";
        }
    }
}