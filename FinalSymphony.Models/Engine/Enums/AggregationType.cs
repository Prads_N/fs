﻿namespace FinalSymphony.Models.Engine.Enums
{
    public enum AggregationType
    {
        Sum = 1,
        Mean = 2,
        Variance = 3,
        StandardDeviation = 4,
        Min = 5,
        Max = 6
    }
}