﻿namespace FinalSymphony.Models.Engine.Enums
{
    public enum ValueDataType
    {
        Keyword = 1,
        SignedInteger = 2,
        UnsignedInteger = 3,
        Double = 4
    }
}