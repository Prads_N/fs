﻿namespace FinalSymphony.Models.Engine
{
    using FinalSymphony.Models.Engine.Enums;

    public class PropertyStorage
    {
        public string PropertyName { get; set; }
        public ValueDataType ValueDataType { get; set; }
        public object Value { get; set; }
    }
}