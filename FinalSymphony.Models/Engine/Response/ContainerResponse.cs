﻿namespace FinalSymphony.Models.Engine.Response
{
    using System;

    public class ContainerResponse
    {
        public ulong Id { get; set; }
        public string Name { get; set; }
        public ulong OwnerId { get; set; }

        public DateTime DateCreated { get; set; }
        public DateTime? DateModified { get; set; }
    }
}