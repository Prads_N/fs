﻿namespace FinalSymphony.Models.Engine.Response
{
    public class QueryResponse
    {
        public object Data { get; set; }
    }
}