﻿namespace FinalSymphony.Models.Engine.Request.Aggregation
{
    using FinalSymphony.Models.Engine.Enums;

    public class AggregationElement
    {
        public string Alias { get; set; }
        public string Property { get; set; }
        public ValueDataType ValueDataType { get; set; }
        public AggregationType AggregationType { get; set; }
    }
}