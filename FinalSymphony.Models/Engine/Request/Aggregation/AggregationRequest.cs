﻿namespace FinalSymphony.Models.Engine.Request.Aggregation
{
    using System.Collections.Generic;

    public class AggregationRequest
    {
        public IEnumerable<AggregationElement> AggregationElements { get; set; }
    }
}