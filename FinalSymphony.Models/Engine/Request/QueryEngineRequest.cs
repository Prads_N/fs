﻿namespace FinalSymphony.Models.Engine.Request
{
    using Data.Enums;
    using Aggregation;

    public class QueryEngineRequest
    {
        public ulong? ContainerId { get; set; }
        public string ContainerName { get; set; }
        public ulong OwnerId { get; set; }
        public string Index { get; set; }

        public ReturnType ReturnType { get; set; }
        public QueryType QueryType { get; set; }
        public object QueryData { get; set; }
        public AggregationRequest AggregationRequest { get; set; }
    }
}