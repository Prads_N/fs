﻿namespace FinalSymphony.Models.Engine.Request
{
    public class ContainerUpdateRequest
    {
        public ulong Id { get; set; }
        public string Name { get; set; }
    }
}