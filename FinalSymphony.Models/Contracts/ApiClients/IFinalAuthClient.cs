﻿namespace FinalSymphony.Models.Contracts.ApiClients
{
    using IdentityModel.Client;
    using System.Threading.Tasks;

    public interface IFinalAuthClient
    {
        Task<TokenResponse> GetToken();
    }
}