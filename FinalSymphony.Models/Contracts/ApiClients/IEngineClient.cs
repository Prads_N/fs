﻿namespace FinalSymphony.Models.Contracts.ApiClients
{
    using Engine.Request;
    using Engine.Response;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface IEngineClient
    {
        Task<IEnumerable<ContainerResponse>> GetContainers(ulong customerId, int page = 1, int limit = 25);
        Task<ContainerResponse> GetContainer(ulong customerId, ulong containerId);
        Task UpdateContainer(ContainerUpdateRequest request);
        Task StoreData(DataStorageEngineRequest request);
        Task<QueryResponse> Query(QueryEngineRequest queryEngineRequest);
    }
}