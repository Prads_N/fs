﻿namespace FinalSymphony.Models.Contracts.Services
{
    using Engine.Request;
    using System.Threading.Tasks;

    public interface IContainerService
    {
        Task UpdateContainer(UserContext userContext, ContainerUpdateRequest request);
    }
}
