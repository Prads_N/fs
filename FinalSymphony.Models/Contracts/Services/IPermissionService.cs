﻿namespace FinalSymphony.Models.Contracts.Services
{
    using System.Threading.Tasks;

    public interface IPermissionService
    {
        Task CheckContainerReadPermission(UserContext userContext, ulong containerId);
        Task CheckContainerWritePermission(UserContext userContext, ulong containerId);
    }
}