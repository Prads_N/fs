﻿namespace FinalSymphony.Models.Contracts.Services
{
    using FinalSymphony.Models.Data;
    using System.Threading.Tasks;

    public interface ICustomerService
    {
        Task<Customer> Insert(Customer customer);
        Task<Customer> GetCustomer(ulong id);
    }
}