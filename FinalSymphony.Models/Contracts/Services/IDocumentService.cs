﻿namespace FinalSymphony.Models.Contracts.Services
{
    using Engine.Request;
    using Engine.Response;
    using Requests;
    using System.Threading.Tasks;

    public interface IDocumentService
    {
        Task Insert(UserContext userContext, DataStorageRequest model);
        Task<QueryResponse> Query(UserContext userContext, QueryEngineRequest request);
    }
}