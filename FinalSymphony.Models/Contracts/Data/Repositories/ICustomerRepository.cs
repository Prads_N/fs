﻿namespace FinalSymphony.Models.Contracts.Data.Repositories
{
    using FinalSymphony.Models.Data;
    using System.Threading.Tasks;

    public interface ICustomerRepository
    {
        Task<Customer> Insert(Customer customer);
        Task<Customer> Get(ulong id);
    }
}