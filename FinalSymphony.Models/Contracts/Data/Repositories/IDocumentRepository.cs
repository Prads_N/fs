﻿namespace FinalSymphony.Models.Contracts.Data.Repositories
{
    using FinalSymphony.Models.Data;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface IDocumentRepository
    {
        Task<DocumentDataModel> Insert(DocumentDataModel model);
        Task<IEnumerable<DocumentDataModel>> Get(ulong customerId, IEnumerable<ulong> documentIds);
    }
}