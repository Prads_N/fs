﻿namespace FinalSymphony.Models.Configurations
{
    public class AppConfiguration
    {
        public DbConnectionStrings DbConnectionStrings { get; set; }
        public ApiEndpoints ApiEndpoints { get; set; }
        public Auth Auth { get; set; }
    }
}