﻿namespace FinalSymphony.Models.Configurations
{
    public class Auth
    {
        public AuthClient Engine { get; set; }
    }

    public class AuthClient
    {
        public string ClientId { get; set; }
        public string ClientSecret { get; set; }
    }
}
