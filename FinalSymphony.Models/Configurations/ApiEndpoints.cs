﻿namespace FinalSymphony.Models.Configurations
{
    public class ApiEndpoints
    {
        public string Auth { get; set; }
        public string Engine { get; set; }
    }
}