﻿namespace FinalSymphony.Models.Configurations
{
    public class DbConnectionStrings
    {
        public string Read { get; set; }
        public string Write { get; set; }
    }
}