﻿namespace FinalSymphony.App.Controllers
{
    using Microsoft.AspNetCore.Mvc;
    using Primitives;
    using System.Diagnostics;

    [Route("")]
    public class HomeController : BaseController
    {
        [HttpGet("")]
        public ActionResult Index()
        {
            var version = FileVersionInfo.GetVersionInfo(typeof(Program).Assembly.Location);
            return Content($"{version.ProductName} - Version: {version.ProductVersion}", "text/html");
        }
    }
}