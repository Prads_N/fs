﻿namespace FinalSymphony.App.Controllers
{
    using FinalSymphony.Models.Contracts.Services;
    using FinalSymphony.Models.Requests;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using Primitives;
    using System.Threading.Tasks;

    [Route("v1/documents"), Authorize]
    public class DocumentController : BaseController
    {
        private readonly IDocumentService _documentService;

        public DocumentController(IDocumentService documentService)
        {
            _documentService = documentService;
        }

        [HttpPost("")]
        public async Task<IActionResult> Store([FromBody]DataStorageRequest request)
        {
            await _documentService.Insert(CurrentUser, request);
            return Ok();
        }
    }
}