﻿namespace FinalSymphony.App.Controllers
{
    using Primitives;
    using Microsoft.AspNetCore.Mvc;
    using Models.Contracts.Services;
    using System.Threading.Tasks;
    using FinalSymphony.Models.Engine.Request;
    using Microsoft.AspNetCore.Authorization;

    [Route("v1/query"), Authorize]
    public class QueryController : BaseController
    {
        private readonly IDocumentService _documentService;

        public QueryController(IDocumentService documentService)
        {
            _documentService = documentService;
        }

        [HttpPost("")]
        public async Task<IActionResult> Query([FromBody]QueryEngineRequest request)
        {
            return Ok(await _documentService.Query(CurrentUser, request));
        }
    }
}