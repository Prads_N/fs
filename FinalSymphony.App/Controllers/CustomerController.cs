﻿namespace FinalSymphony.App.Controllers
{
    using Primitives;
    using Models.Contracts.Services;
    using Microsoft.AspNetCore.Mvc;
    using System.Threading.Tasks;
    using FinalSymphony.Models.Requests;
    using FinalSymphony.Models.Data;
    using Microsoft.AspNetCore.Authorization;

    [Route("v1/customers"), Authorize]
    public class CustomerController : BaseController
    {
        private readonly ICustomerService _customerService;

        [HttpPost("")]
        public Task Create(CreateCustomerRequest request)
        {
            return _customerService.Insert(new Customer
            {
                Name = request.Name,
                Status = request.Status
            });
        }
    }
}