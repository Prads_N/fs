﻿namespace FinalSymphony.App.Controllers
{
    using Primitives;
    using Models.Contracts.Services;
    using Microsoft.AspNetCore.Mvc;
    using System.Threading.Tasks;
    using FinalSymphony.Models.Engine.Request;
    using Microsoft.AspNetCore.Authorization;

    [Route("v1/containers"), Authorize]
    public class ContainerController : BaseController
    {
        private readonly IContainerService _containerService;

        public ContainerController(IContainerService containerService)
        {
            _containerService = containerService;
        }

        [HttpPut("")]
        public async Task<IActionResult> Update([FromBody]ContainerUpdateRequest request)
        {
            await _containerService.UpdateContainer(CurrentUser, request);
            return Ok();
        }
    }
}