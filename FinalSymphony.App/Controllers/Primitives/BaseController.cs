﻿namespace FinalSymphony.App.Controllers.Primitives
{
    using FinalSymphony.Models;
    using FinalSymphony.Models.Exceptions;
    using Microsoft.AspNetCore.Mvc;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Security.Claims;
    using System.Security.Principal;

    public abstract class BaseController : ControllerBase
    {
        protected UserContext CurrentUser {
            get
            {
                var principal = this.User;

                var claims = GetClaimsPrincipal(principal)?.Claims;

                if (claims == null || !claims.Any())
                    throw new NotAuthorizedException();

                return new UserContext
                {
                    CustomerId = GetValue(claims, Constants.Auth.Claims.CustomerId, UInt64.Parse)
                };
            }
        }

        private static ClaimsPrincipal GetClaimsPrincipal(IPrincipal principal)
        {
            var user = principal as ClaimsPrincipal;

            return user?.Claims == null ? null : user;
        }

        private static T GetValue<T>(IEnumerable<Claim> claims, string key, Func<string, T> fn)
        {
            var val = claims.FirstOrDefault(c => c.Type == key)?.Value;

            if (val == null)
                throw new NotAuthorizedException();

            return fn.Invoke(val);
        }
    }
}