﻿namespace FinalSymphony.App.SerilogEnrichers
{
    using System.Diagnostics;
    using System.Reflection;
    using Serilog.Core;
    using Serilog.Events;

    public class ApplicationDetailEnricher : ILogEventEnricher
    {
        private static readonly string _productName;
        private static readonly string _productVersion;

        static ApplicationDetailEnricher()
        {
            var assembly = Assembly.GetEntryAssembly();
            var version = FileVersionInfo.GetVersionInfo(assembly.Location);

            _productName = version.ProductName;
            _productVersion = version.ProductVersion;
        }

        public void Enrich(LogEvent logEvent, ILogEventPropertyFactory propertyFactory)
        {
            logEvent.AddOrUpdateProperty(propertyFactory.CreateProperty("ProductName", _productName));
            logEvent.AddOrUpdateProperty(propertyFactory.CreateProperty("ProductVersion", _productVersion));
        }
    }
}