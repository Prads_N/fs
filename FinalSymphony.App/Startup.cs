﻿namespace FinalSymphony.App
{
    using Models.Configurations;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using FinalSymphony.Models.Contracts.Services;
    using FinalSymphony.Services;
    using FinalSymphony.Models.Contracts.Data.Repositories;
    using FinalSymphony.Data.Repositories;
    using FinalSymphony.Models.Contracts.Data;
    using FinalSymphony.Data;
    using FinalSymphony.Models.Contracts.ApiClients;
    using FinalSymphonyApiClients;
    using Microsoft.AspNetCore.Authentication.JwtBearer;
    using FinalSymphony.Models;
    using Microsoft.AspNetCore.Authorization;
    using FinalSymphony.ApiClients;
    using FinalSymphony.App.Middlewares;

    public class Startup
    {
        private readonly IConfiguration _configuration;

        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            var config = _configuration.Get<AppConfiguration>();

            services.AddMvcCore().AddAuthorization().AddJsonFormatters();
            AddAuth(services, config);

            #region Repositories

            services.AddSingleton<ICustomerRepository, CustomerRepository>();
            services.AddSingleton<IDocumentRepository, DocumentRepository>();

            #endregion

            #region Services

            services.AddSingleton<ICustomerService, CustomerService>();
            services.AddSingleton<IContainerService, ContainerService>();
            services.AddSingleton<IDocumentService, DocumentService>();
            services.AddSingleton<IPermissionService, PermissionService>();

            #endregion

            #region DB Connection

            services.AddSingleton<IDbConnectionFactory>(new DbConnectionFactory(config.DbConnectionStrings.Read, config.DbConnectionStrings.Write));

            #endregion

            #region Api Clients

            services.AddSingleton<IEngineClient>(p => new EngineClient(config.ApiEndpoints.Engine, p.GetRequiredService<IFinalAuthClient>()));
            services.AddSingleton<IFinalAuthClient>(new FinalAuthClient(config.ApiEndpoints.Auth, config.Auth.Engine.ClientId, config.Auth.Engine.ClientSecret));

            #endregion
        }

        public void Configure(IApplicationBuilder app)
        {
            app.UseAuthentication();
            app.UseMiddleware<ExceptionMiddleware>();
            app.UseMvc();
        }

        private void AddAuth(IServiceCollection services, AppConfiguration config)
        {
            services.AddAuthentication(o =>
            {
                o.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                o.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddIdentityServerAuthentication(o =>
            {
                o.Authority = config.ApiEndpoints.Auth;
                o.ApiName = Constants.Auth.ApiNames.Client;
            });

            services.AddAuthorization(o =>
            {
                o.DefaultPolicy = new AuthorizationPolicyBuilder()
                    .RequireScope(Constants.Auth.Scopes.Client)
                    .Build();
            });
        }
    }
}