﻿namespace FinalSymphony.App.Middlewares
{
    using Models.Exceptions;
    using Microsoft.AspNetCore.Http;
    using Microsoft.Extensions.Logging;
    using System;
    using System.Net;
    using System.Threading.Tasks;
    using Utils.Extensions;

    public class ExceptionMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger _logger;

        public ExceptionMiddleware(RequestDelegate next, ILogger<ExceptionMiddleware> logger)
        {
            _next = next;
            _logger = logger;
        }

        public async Task InvokeAsync(HttpContext httpContext)
        {
            try
            {
                await _next(httpContext);
            }
            catch (ClientErrorException ex)
            {
                await HandleClientException(httpContext, ex);
            }
            catch (Exception ex)
            {
                await HandleGenericException(httpContext, ex);
            }
        }

        private Task HandleClientException(HttpContext httpContext, Exception exception)
        {
            httpContext.Response.ContentType = "application/json";
            httpContext.Response.StatusCode = (int)HttpStatusCode.BadRequest;

            return httpContext.Response.WriteAsync(new
            {
                exception.Message
            }.ToJson());
        }

        private Task HandleGenericException(HttpContext httpContext, Exception exception)
        {
            _logger.LogError(exception, "Error occurred");

            httpContext.Response.ContentType = "application/json";
            httpContext.Response.StatusCode = (int)HttpStatusCode.InternalServerError;

            return httpContext.Response.WriteAsync(new
            {
                exception.Message
            }.ToJson());
        }
    }
}