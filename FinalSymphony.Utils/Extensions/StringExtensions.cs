﻿namespace FinalSymphony.Utils.Extensions
{
        using Newtonsoft.Json;

    public static class StringExtensions
    {
        public static TOutput JsonToObject<TOutput>(this string value)
        {
            return JsonConvert.DeserializeObject<TOutput>(value);
        }
    }
}