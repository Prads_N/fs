﻿namespace FinalSymphony.Utils.Extensions
{
    using Newtonsoft.Json;

    public static class ObjectExtensions
    {
        public static T Convert<T>(this object obj)
        {
            return JsonConvert.DeserializeObject<T>(JsonConvert.SerializeObject(obj));
        }

        public static string ToJson(this object obj)
        {
            return JsonConvert.SerializeObject(obj);
        }
    }
}