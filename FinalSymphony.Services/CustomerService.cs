﻿namespace FinalSymphony.Services
{
    using FinalSymphony.Models.Data;
    using Models.Contracts.Data.Repositories;
    using Models.Contracts.Services;
    using System.Threading.Tasks;

    public class CustomerService : ICustomerService
    {
        private readonly ICustomerRepository _customerRepository;

        public CustomerService(ICustomerRepository customerRepository)
        {
            _customerRepository = customerRepository;
        }

        public Task<Customer> Insert(Customer customer)
        {
            return _customerRepository.Insert(customer);
        }

        public Task<Customer> GetCustomer(ulong id)
        {
            return _customerRepository.Get(id);
        }
    }
}