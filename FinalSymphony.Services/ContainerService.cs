﻿namespace FinalSymphony.Services
{
    using Models;
    using Models.Engine.Request;
    using Models.Contracts.ApiClients;
    using Models.Contracts.Services;
    using System.Threading.Tasks;

    public class ContainerService : IContainerService
    {
        private readonly IEngineClient _engineClient;
        private readonly IPermissionService _permissionService;

        public ContainerService(IEngineClient engineClient, IPermissionService permissionService)
        {
            _engineClient = engineClient;
            _permissionService = permissionService;
        }

        public async Task UpdateContainer(UserContext userContext, ContainerUpdateRequest request)
        {
            await _permissionService.CheckContainerWritePermission(userContext, request.Id);
            await _engineClient.UpdateContainer(request);
        }
    }
}