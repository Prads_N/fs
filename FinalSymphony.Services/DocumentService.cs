﻿namespace FinalSymphony.Services
{
    using Models;
    using Models.Contracts.ApiClients;
    using Models.Data;
    using Models.Requests;
    using Models.Contracts.Data.Repositories;
    using Models.Contracts.Services;
    using System.Threading.Tasks;
    using System.Linq;
    using Models.Exceptions;
    using System;
    using Models.Engine.Response;
    using Models.Engine.Request;
    using FinalSymphony.Models.Data.Enums;
    using System.Collections.Generic;
    using FinalSymphony.Utils.Extensions;
    using Newtonsoft.Json;

    public class DocumentService : IDocumentService
    {
        private readonly IDocumentRepository _documentRepository;
        private readonly IPermissionService _permissionService;
        private readonly IEngineClient _engineClient;

        public DocumentService(IDocumentRepository documentRepository, IPermissionService permissionService, IEngineClient engineClient)
        {
            _documentRepository = documentRepository;
            _permissionService = permissionService;
            _engineClient = engineClient;
        }

        public async Task Insert(UserContext userContext, DataStorageRequest request)
        {
            if (!request.EngineRequest.Properties.Any())
                throw new ClientErrorException("Properties cannot be empty");

            if (request.EngineRequest.ContainerId.HasValue)
                await _permissionService.CheckContainerWritePermission(userContext, request.EngineRequest.ContainerId.Value);
            else if (String.IsNullOrWhiteSpace(request.EngineRequest.ContainerName))
                throw new ClientErrorException("Container information is missing");

            request.EngineRequest.OwnerId = userContext.CustomerId;
            request.EngineRequest.DocumentId = (await _documentRepository.Insert(new DocumentDataModel
            {
                CustomerId = userContext.CustomerId,
                DataJson = request.DataJson
            })).Id;
            request.EngineRequest.Index = Constants.Elastic.Index;

            await _engineClient.StoreData(request.EngineRequest);
        }

        public async Task<QueryResponse> Query(UserContext userContext, QueryEngineRequest request)
        {
            if (request.ContainerId.HasValue)
                await _permissionService.CheckContainerWritePermission(userContext, request.ContainerId.Value);
            else if (String.IsNullOrWhiteSpace(request.ContainerName))
                return new QueryResponse { Data = null };

            request.OwnerId = userContext.CustomerId;
            request.Index = Constants.Elastic.Index;

            var engineResponse = await _engineClient.Query(request);

            if (request.ReturnType == ReturnType.Extraction)
                return await this.GetExtraction(userContext, engineResponse);

            return engineResponse;
        }

        private async Task<QueryResponse> GetExtraction(UserContext userContext, QueryResponse engineResponse)
        {
            var documentIds = engineResponse.Data.Convert<IEnumerable<ulong>>();

            if (!documentIds.Any())
            {
                return new QueryResponse
                {
                    Data = Enumerable.Empty<object>()
                };
            }

            return new QueryResponse
            {
                Data = (await _documentRepository.Get(userContext.CustomerId, documentIds))
                    .Select(s => JsonConvert.DeserializeObject(s.DataJson))
            };
        }
    }
}