﻿namespace FinalSymphony.Services
{
    using FinalSymphony.Models;
    using FinalSymphony.Models.Exceptions;
    using Models.Contracts.ApiClients;
    using Models.Contracts.Services;
    using System.Threading.Tasks;

    public class PermissionService : IPermissionService
    {
        private readonly IEngineClient _engineClient;

        public PermissionService(IEngineClient engineClient)
        {
            _engineClient = engineClient;
        }

        public async Task CheckContainerReadPermission(UserContext userContext, ulong containerId)
        {
            //We can probably cache this
            var container = await _engineClient.GetContainer(userContext.CustomerId, containerId);

            //If the container is null, then this customer doesn't have access to the container
            if (container == null)
                throw new PermissionException("You don't have permission to access the container");
        }

        public Task CheckContainerWritePermission(UserContext userContext, ulong containerId)
        {
            //If they don't have read permission, they don't have write permission either
            return this.CheckContainerReadPermission(userContext, containerId);
        }
    }
}