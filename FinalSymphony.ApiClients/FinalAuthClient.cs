﻿namespace FinalSymphony.ApiClients
{
    using FinalSymphony.Models;
    using FinalSymphony.Models.Contracts.ApiClients;
    using IdentityModel.Client;
    using System;
    using System.Net.Http;
    using System.Threading.Tasks;

    public class FinalAuthClient : IFinalAuthClient
    {
        private readonly HttpClient _tokenClient;
        private readonly string _endpoint;
        private readonly string _clientId;
        private readonly string _clientSecret;

        public FinalAuthClient(string endpoint, string clientId, string clientSecret)
        {
            _tokenClient = new HttpClient();
            _endpoint = $"{endpoint.TrimEnd('/')}/connect/token";
            _clientId = clientId;
            _clientSecret = clientSecret;
        }

        public async Task<TokenResponse> GetToken()
        {
            var response = await _tokenClient.RequestClientCredentialsTokenAsync(new ClientCredentialsTokenRequest
            {
                Address = _endpoint,
                ClientId = _clientId,
                ClientSecret = _clientSecret,
                Scope = Constants.Auth.Scopes.Engine
            });

            if (response.IsError)
                throw new Exception(response.Error);

            return response;
        }
    }
}