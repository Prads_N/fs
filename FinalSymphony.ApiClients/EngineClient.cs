﻿namespace FinalSymphonyApiClients
{
    using FinalSymphony.Models.Contracts.ApiClients;
    using FinalSymphony.Models.Engine.Response;
    using FinalSymphony.ApiClients.Primitives;
    using RestSharp;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using FinalSymphony.Models.Engine.Request;

    public class EngineClient : BaseAuthenticatedApiClient, IEngineClient
    {
        public EngineClient(string endpoint, IFinalAuthClient finalAuthClient) : base(endpoint, finalAuthClient) { }

        public Task<IEnumerable<ContainerResponse>> GetContainers(ulong customerId, int page = 1, int limit = 25)
        {
            var request = new RestRequest("v1/owners/{customerId}/containers", Method.GET);

            request.AddUrlSegment(nameof(customerId), customerId);
            request.AddQueryParameter(nameof(page), page.ToString());
            request.AddQueryParameter(nameof(limit), limit.ToString());

            return Execute<IEnumerable<ContainerResponse>>(request);
        }

        public Task<ContainerResponse> GetContainer(ulong customerId, ulong containerId)
        {
            var request = new RestRequest("v1/owners/{customerId}/containers/{containerId}", Method.GET);

            request.AddUrlSegment(nameof(customerId), customerId);
            request.AddUrlSegment(nameof(containerId), containerId);

            return Execute<ContainerResponse>(request);
        }

        public Task UpdateContainer(ContainerUpdateRequest containerUpdateRequest)
        {
            var request = new RestRequest("v1/containers", Method.PUT);

            request.AddJsonBody(containerUpdateRequest);

            return Execute(request);
        }

        public Task StoreData(DataStorageEngineRequest dataStorageEngineRequest)
        {
            var request = new RestRequest("v1/documents", Method.POST);

            request.AddJsonBody(dataStorageEngineRequest);

            return Execute(request);
        }

        public Task<QueryResponse> Query(QueryEngineRequest queryEngineRequest)
        {
            var request = new RestRequest("v1/query", Method.POST);

            request.AddJsonBody(queryEngineRequest);

            return Execute<QueryResponse>(request);
        }
    }
}