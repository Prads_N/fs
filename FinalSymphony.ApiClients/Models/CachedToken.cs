﻿namespace FinalSymphony.ApiClients.Models
{
    using IdentityModel.Client;
    using System;

    public class CachedToken
    {
        public string AccessToken { get; }
        public DateTime Expiry { get; }

        public bool HasExpired => Expiry <= DateTime.UtcNow;

        public CachedToken(TokenResponse token)
        {
            this.AccessToken = token.AccessToken;
            this.Expiry = DateTime.UtcNow.AddSeconds(token.ExpiresIn - 60);
        }
    }
}