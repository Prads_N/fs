﻿namespace FinalSymphony.ApiClients.Primitives
{
    using Utils.Extensions;
    using RestSharp;
    using System;
    using System.Threading.Tasks;
    using FinalSymphony.Models.Exceptions;
    using FinalSymphony.ApiClients.Serialisers;

    public abstract class BaseApiClient
    {
        private readonly IRestClient _client;

        protected BaseApiClient(string endpoint)
        {
            _client = new RestClient(endpoint)
                .UseSerializer<JsonSerialiser>();
        }

        protected virtual async Task<TOutput> Execute<TOutput>(RestRequest request)
        {
            var response = await _client.ExecuteTaskAsync(request);

            if (response.IsSuccessful)
                return response.Content.JsonToObject<TOutput>();

            this.ThrowOnError(response);
            return default(TOutput); //This will never be executed because the above will throw
        }

        protected virtual async Task Execute(RestRequest request)
        {
            var response = await _client.ExecuteTaskAsync(request);

            if (response.IsSuccessful)
                return;

            this.ThrowOnError(response);
        }

        private void ThrowOnError(IRestResponse response)
        {
            if ((int)response.StatusCode >= 400 && (int)response.StatusCode <= 499)
                throw new ClientErrorException(response.ErrorMessage, response.ErrorException);

            throw new Exception(response.ErrorMessage, response.ErrorException);
        }
    }
}