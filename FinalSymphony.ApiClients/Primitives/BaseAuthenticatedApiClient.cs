﻿namespace FinalSymphony.ApiClients.Primitives
{
    using System.Threading;
    using System.Threading.Tasks;
    using FinalSymphony.ApiClients.Models;
    using FinalSymphony.Models.Contracts.ApiClients;
    using RestSharp;

    public abstract class BaseAuthenticatedApiClient : BaseApiClient
    {
        private readonly IFinalAuthClient _finalAuthClient;

        private CachedToken _cachedToken;
        private readonly SemaphoreSlim _cachedTokenSemaphore;

        protected BaseAuthenticatedApiClient(string endpoint, IFinalAuthClient finalAuthClient) : base(endpoint)
        {
            _finalAuthClient = finalAuthClient;
            _cachedTokenSemaphore = new SemaphoreSlim(1, 1);
        }

        protected override async Task Execute(RestRequest request)
        {
            await this.AddBearToken(request);
            await base.Execute(request);
        }

        protected override async Task<TOutput> Execute<TOutput>(RestRequest request)
        {
            await this.AddBearToken(request);
            return await base.Execute<TOutput>(request);
        }

        private async Task AddBearToken(RestRequest request)
        {
            if (_cachedToken == null || _cachedToken.HasExpired)
                await this.GetNewToken();

            request.AddHeader("Authorization", $"Bearer {_cachedToken.AccessToken}");
        }

        private async Task GetNewToken()
        {
            await _cachedTokenSemaphore.WaitAsync();

            try
            {
                if (_cachedToken == null || _cachedToken.HasExpired)
                    _cachedToken = new CachedToken(await _finalAuthClient.GetToken());
            }
            finally
            {
                _cachedTokenSemaphore.Release();
            }
        }
    }
}